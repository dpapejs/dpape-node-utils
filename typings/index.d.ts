export interface requestOptions {
    url: string;
    method?: 'POST' | 'GET' | 'PUT' | 'OPTIONS' | 'DELETE';
    data?: {
        [key: string]: any;
    };
    hostname: string;
    port: number;
    headers?: {
        [key: string]: any;
    };
}
/**
* Compressed file function configuration item
*/
interface zipOptions {
    ignore?: string[];
}
interface aesOption {
    /** decrypt password */
    key?: string;
    /** Offset during encryption */
    iv?: string;
}
/**
 * Date formatting
 */
export declare function dateFormat(value: string | number | Date, format?: string): string | number | Date | undefined;
/**
 * MD5 Encrypt the string
 * @param {*} val
 * @returns
 */
export declare function md5(val?: string): string;
/**
 * Whether the value is invalid. eg: undefined | null | empty character string | NaN
 */
export declare function isInvalid(val: any): number | true;
/**
 * Generate unique identifiers
 * @param digit Generate value digit
 * @returns {string}
 */
export declare function generateUniqueId(digit?: number): string;
/**
 * Get the cookie value
 * @param cookie cookie value
 * @param key
 * @returns {string | null}
 */
export declare function getCookie(cookie: string, key: string): string | null;
/**
 * Get the local IP address
 * @param type {string}
 * @returns
 */
export declare function getLocalIPAddress(type?: "IPv4" | "IPv6"): string;
/**
 * Send http request
 * @param options
 * @returns {Promise<any>}
 */
export declare function request(options: requestOptions): Promise<any>;
/**
 * Delete the directory and all subfiles
 */
export declare function removeDir(dirPath: string, callback: () => void): void;
/**
* Folder compression
* @param filename
* @param folderPath
* @returns
*/
export declare function zip(filename: string, folderPath: string, savePath: string, options?: zipOptions): Promise<any>;
/**
* Decompress the ZIP file
* @param filePath Zip file path
* @param output File output path
*/
export declare function unzip(filePath: string, output: string): Promise<unknown>;
/**
 * Beautify console type output
 * @param type output type
 * @param typeMsg type message info
 * @param msg Output message
 */
export declare function consoleBeautify(msg: string, type?: "INFO" | "WARNING" | "ERROR" | 'SUCCESS', typeMsg?: string): void;
/**
 * Decimal point interception
 * @param val value
 * @param digit decimal place
 * @param round Whether to round
 */
export declare function toDecimal(val: string | number, digit?: number, round?: boolean): string;
/**
 * Open the url to browser
 * @param url URL link
 */
export declare function openURLBrowser(url: string): void;
/**
 * AES_128_CBC encryption
 * @param value Decryption value
 * @param options Decryption parameters
 */
export declare function enCryp(value: string, options?: aesOption): string;
/**
* AES_128_CBC decryption
* @param value Decryption value
* @param options Decryption parameters
*/
export declare function deCryp(value: string, options?: aesOption): string;
/**
 * Get the currently available port (non-occupied)
 * @param port value range 0~65535
 */
export declare function getAvailablePort(port?: number, hostname?: string): Promise<number>;
/**
 * Object data deep replication
 */
export declare function objectDeepCopy(data?: {
    [key: string]: any;
} | {
    [key: string]: any;
}[]): {
    [key: string]: any;
};
declare const _default: {
    dateFormat: typeof dateFormat;
    isInvalid: typeof isInvalid;
    md5: typeof md5;
    generateUniqueId: typeof generateUniqueId;
    getCookie: typeof getCookie;
    getLocalIPAddress: typeof getLocalIPAddress;
    request: typeof request;
    unzip: typeof unzip;
    zip: typeof zip;
    removeDir: typeof removeDir;
    consoleBeautify: typeof consoleBeautify;
    toDecimal: typeof toDecimal;
    openURLBrowser: typeof openURLBrowser;
    enCryp: typeof enCryp;
    deCryp: typeof deCryp;
    getAvailablePort: typeof getAvailablePort;
    objectDeepCopy: typeof objectDeepCopy;
};
export default _default;
