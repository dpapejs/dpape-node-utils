const { join } = require("path");
const { copyFileSync } = require("fs");
require("esbuild").buildSync({
  entryPoints: ["src/index.ts"],
  bundle: true,
  platform: "node",
  target: ["node10.4"],
  external: ["jszip"],
  minify: true,
  outfile: "lib/index.js",
});
const targetPath = join(__dirname, "../lib");
const typingsPath = join(__dirname, "../typings");
copyFileSync(
  join(typingsPath, "./index.d.ts"),
  join(targetPath, "./index.d.ts")
);
