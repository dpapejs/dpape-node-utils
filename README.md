# @dpapejs/node-utils

nodejs common tool library

## Install

```
npm i @dpapejs/node-utils -S
```

## Quick Start

```js
// Delete the directory and all subfiles
const { removeDir } = require("@dpapejs/node-utils");
const path = require("path");
removeDir(path.join(__dirname, "./test"), () => {
  console.log("Successful");
});
```
