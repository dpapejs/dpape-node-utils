import { createHash, createDecipheriv, createCipheriv } from 'crypto'
import { networkInterfaces } from 'os'
import { createServer } from 'net'
import * as http from 'http'
import * as fs from 'fs';
import { join, resolve } from 'path'
var JSZip = require('jszip')

export interface requestOptions {
  url: string
  method?: 'POST' | 'GET' | 'PUT' | 'OPTIONS' | 'DELETE'
  data?: { [key: string]: any }
  hostname: string
  port: number
  headers?: { [key: string]: any }
}

/**
* Compressed file function configuration item
*/
interface zipOptions {
  ignore?: string[]
}

interface aesOption {
  /** decrypt password */
  key?: string
  /** Offset during encryption */
  iv?: string
}

/**
 * Date formatting
 */
export function dateFormat(value: string | number | Date, format: string = 'yyyy-MM-dd hh:mm:ss') {
  let dateValue: Date | undefined = undefined
  let isInvalidDate = false
  if (typeof value === 'string') {
    const transferredStr = value.replace(/-/g, '\/')
    dateValue = new Date(transferredStr)
    isInvalidDate = dateValue.toString() === 'Invalid Date'
  }
  if (isInvalidDate) return value
  if (typeof value === 'number') dateValue = new Date(value)
  if (!dateValue) dateValue = value as Date
  if (!dateValue) return
  const year = dateValue.getFullYear()
  const month = dateValue.getMonth()
  const day = dateValue.getDate()
  const hour = dateValue.getHours()
  const minute = dateValue.getMinutes()
  const second = dateValue.getSeconds()
  const leftPad = (val: number) => {
    return val >= 10 ? `${val}` : `0${val}`
  }
  const values: { [key: string]: string } = {
    'yyyy': year.toString(),
    'MM': leftPad(month + 1),
    'dd': leftPad(day),
    'hh': leftPad(hour),
    'mm': leftPad(minute),
    'ss': leftPad(second)
  }
  let result = format
  Object.keys(values).forEach(key => {
    result = result.replace(new RegExp(key, 'g'), values[key])
  })
  return result
}


/**
 * MD5 Encrypt the string
 * @param {*} val
 * @returns
 */
export function md5(val: string = '') {
  if (!val) return val
  return createHash('md5').update(val, 'utf-8').digest('hex')
}

/**
 * Whether the value is invalid. eg: undefined | null | empty character string | NaN
 */
export function isInvalid(val: any) {
  return val === undefined || val === '' || val === null || NaN
}

/**
 * Generate unique identifiers
 * @param digit Generate value digit
 * @returns {string}
 */
export function generateUniqueId(digit = 16): string {
  const length = parseInt(`${digit / 15}`) + ((digit % 15 === 0) ? 0 : 1)
  const uuid = Array.from({ length })
    .map(() => {
      return (Math.random() * 1000).toString(36).replace(/\./g, "");
    })
    .join("")
    .substring(0, digit);
  return uuid
}

/**
 * Get the cookie value  
 * @param cookie cookie value
 * @param key 
 * @returns {string | null}
 */
export function getCookie(cookie: string, key: string): string | null {
  const reg = new RegExp('(^| )' + key + '=([^;]*)(;|$)')
  const arr = cookie.match(reg)
  return arr ? decodeURI(arr[2]) : null
}

/**
 * Get the local IP address
 * @param type {string}
 * @returns 
 */
export function getLocalIPAddress(type: "IPv4" | "IPv6" = 'IPv4') {
  const interfaces = networkInterfaces()
  const options = Object.keys(interfaces).map(item => {
    const val = interfaces[item]
    return val instanceof Array ? val : [val]
  }).flat()
  const result = options.find(item => item && item.family === type && !item.internal && item.address !== '127.0.0.1')
  return result ? result.address : ''
}

/**
 * Send http request
 * @param options 
 * @returns {Promise<any>}
 */
export function request(options: requestOptions): Promise<any> {
  return new Promise((resolve, reject) => {
    const {
      url,
      data,
      hostname,
      method,
      port,
      headers
    } = options
    const content = []
    const sendData = data || {}
    Object.keys(sendData).forEach(key => {
      content.push(`${key}=${sendData[key]}`)
    })
    const headersData = Object.assign({}, {
      'Content-Type': "application/json; charset=utf-8"
    }, headers || {})
    var main = {
      hostname: hostname,
      port: port,  //注意端口号 可以忽略不写 写一定写对
      path: url,
      method: method || "GET",
      headers: headersData
    };

    var req = http.request(main, function (res) {
      res.setEncoding('utf8');
      let chunkData = ''
      res.on('end', () => {
        resolve(chunkData)
      })
      res.on('data', function (chunk) {
        chunkData += chunk
      });
    });

    req.on('error', function (e) {
      reject(e)
    });
    if (method === 'POST' || method === 'PUT') req.write(JSON.stringify(data));
    req.end();
  })
}

/**
 * Delete the directory and all subfiles
 */
export function removeDir(dirPath: string, callback: () => void) {
  try {
    if (!fs.existsSync(dirPath) || !fs.statSync(dirPath).isDirectory()) {
      callback && callback()
      return
    }
    const files = fs.readdirSync(dirPath)
    if (files.length === 0) {
      fs.rmdirSync(dirPath)
      callback && callback()
      return
    }
    const maxLength = files.length
    let len = 0
    const done = (index: number) => {
      if (index !== maxLength) return
      fs.rmdirSync(dirPath)
      callback && callback()
    }
    files.forEach(file => {
      const curPath = join(dirPath, file)
      if (fs.statSync(curPath).isDirectory()) {
        removeDir(curPath, () => {
          done(len += 1)
        })
        return
      }
      fs.unlinkSync(curPath)
      done(len += 1)
    })
  } catch (err) {
    console.log('[ERROR_LOG]\r\n')
    console.log(err)
    throw Error()
  }
}
/**
* Folder compression
* @param filename 
* @param folderPath 
* @returns 
*/
export async function zip(filename: string, folderPath: string, savePath: string, options: zipOptions = {}) {
  if (!fs.existsSync(folderPath) || !fs.statSync(folderPath).isDirectory()) {
    throw Error('invalid_path')
  }
  const zip = new JSZip()
  const { ignore } = options
  function addFiles(pPath: string, floder?: string) {
    const files = fs.readdirSync(pPath, { withFileTypes: true })
    files.forEach((dirent) => {
      if (ignore && ignore.indexOf(dirent.name) >= 0) return
      const filePath = join(pPath, dirent.name)
      if (dirent.isDirectory()) {
        addFiles(filePath, `${floder || ''}${dirent.name}/`)
        return
      }
      const name = `${floder || ''}${dirent.name}`
      zip.file(name, fs.readFileSync(filePath))
    })
  }
  addFiles(folderPath)
  return zip.generateAsync({
    type: 'nodebuffer',
    compression: 'DEFLATE',
    compressionOptions: {
      level: 9
    }
  }).then(function (content: any) {
    if (!fs.existsSync(savePath)) fs.mkdirSync(savePath, { recursive: true })
    fs.writeFileSync(resolve(savePath, `./${filename}.zip`), content)
  })
}

/**
* Decompress the ZIP file  
* @param filePath Zip file path  
* @param output File output path
*/
export async function unzip(filePath: string, output: string) {
  return new Promise((resolve, reject) => {
    if (!filePath || !fs.existsSync(filePath)) {
      reject('The decompressed file does not exist')
      return
    }
    const file = fs.readFileSync(filePath)
    if (!fs.existsSync(output)) fs.mkdirSync(output, { recursive: true })
    JSZip.loadAsync(file, { optimizedBinaryString: true }).then((res: any) => {
      const files = res.files
      const keys = Object.keys(files)
      let index = 0
      const callback = () => {
        index += 1
        if (index !== keys.length) return
        resolve(true)
      }
      keys.forEach((file: any) => {
        const item = files[file]
        const dir = item.dir
        const filePath = join(output, file)
        const existsSync = fs.existsSync(filePath)
        if (dir && !existsSync) {
          fs.mkdirSync(filePath, { recursive: true })
          callback()
          return
        }
        if (dir) {
          callback()
          return
        }
        res.file(file).async('string')
          .then((content: string | NodeJS.ArrayBufferView) => {
            fs.writeFileSync(filePath, content)
            callback()
          }).catch(reject)

      })
    })
  })
}

/**
 * Beautify console type output
 * @param type output type
 * @param typeMsg type message info
 * @param msg Output message
 */
export function consoleBeautify(msg: string, type: "INFO" | "WARNING" | "ERROR" | 'SUCCESS' = 'INFO', typeMsg?: string) {
  const typeValue = {
    INFO: `\x1b[44m\x1b[30m ${typeMsg || 'INFO'} \x1b[0m`,
    WARNING: `\x1b[43m\x1b[30m ${typeMsg || 'WARNING'} \x1b[0m`,
    ERROR: `\x1b[41m\x1b[30m ${typeMsg || 'ERROR'} \x1b[0m`,
    SUCCESS: `\x1b[42m\x1b[30m ${typeMsg || 'SUCCESS'} \x1b[0m`
  }
  const showType = typeValue[type]
  console.log(showType, msg)
}

/**
 * Decimal point interception
 * @param val value
 * @param digit decimal place 
 * @param round Whether to round
 */
export function toDecimal(val: string | number, digit: number = 2, round: boolean = true) {
  if (val === '' || val === undefined || val === null) return val
  /**
   * Native method processing
   */
  if (round) {
    return (typeof val === 'string' ? parseFloat(val) : val).toFixed(digit)
  }
  const result = typeof val === 'number' ? val.toString() : val
  /**
   * zeroize
   */
  const numberPad = (length: number) => {
    return Array.from({ length }).map(() => 0).join('')
  }
  /**
   * No decimal point
   */
  if (result.indexOf('.') < 0) {
    return `${result}.${numberPad(digit)}`
  }
  const splits = result.split('.')
  const baseValue = splits[0]
  const decimal = splits[1]
  const padLength = digit - decimal.length
  return `${baseValue}.${decimal}${numberPad(padLength <= 0 ? 0 : padLength)}`
}

/**
 * Open the url to browser
 * @param url URL link
 */
export function openURLBrowser(url: string) {
  if (!url) {
    throw Error('[ERROR] url is empty!')
  }
  var exec = require('child_process').exec;
  switch (process.platform) {
    case "darwin":
      exec('open ' + url);
      break;
    case "win32":
      exec('start ' + url);
      break;
    default:
      exec('xdg-open', [url]);
  }
}

/**
 * AES_128_CBC encryption
 * @param value Decryption value
 * @param options Decryption parameters
 */
export function enCryp(value: string, options: aesOption = {}) {
  const { key, iv } = options
  if (!key || !iv || key.length !== iv.length) {
    throw Error('Invalid options data')
  }
  if (key.length !== iv.length) {
    throw Error('Key and IV have different lengths')
  }
  const cipherChunks = []
  const cipher = createCipheriv('aes-128-cbc', key, iv)
  cipher.setAutoPadding(true)
  cipherChunks.push(cipher.update(value, 'utf8', 'base64'))
  cipherChunks.push(cipher.final('base64'))
  return cipherChunks.join('')
}

/**
* AES_128_CBC decryption
* @param value Decryption value
* @param options Decryption parameters
*/
export function deCryp(value: string, options: aesOption = {}) {
  const { key, iv } = options
  if (!key || !iv || key.length !== iv.length) {
    throw Error('Invalid options data')
  }
  if (key.length !== iv.length) {
    throw Error('Key and IV have different lengths')
  }
  const cipherChunks = []
  const decipher = createDecipheriv('aes-128-cbc', key, iv)
  decipher.setAutoPadding(true)
  cipherChunks.push(decipher.update(value, 'base64', 'utf8'))
  cipherChunks.push(decipher.final('utf8'))
  return cipherChunks.join('')
}

/**
 * Get the currently available port (non-occupied)
 * @param port value range 0~65535
 */
export async function getAvailablePort(port: number = 8080, hostname: string = '0.0.0.0'): Promise<number> {
  return new Promise((resolve, reject) => {
    if (port > 65535) {
      reject();
      throw Error("[Error] Invalid port");
    }
    const server = createServer().listen(port, hostname);
    server.on('listening', function () {
      server.close();
      resolve(port);
    });
    server.on('error', () => {
      server.close();
      getAvailablePort(port + 1, hostname).then(resolve)
    });
  });
}

/**
 * Object data deep replication
 */
export function objectDeepCopy(data: { [key: string]: any } | { [key: string]: any }[] = {}) {
  if (typeof data !== 'object') return data
  const fn = (data: { [key: string]: any }) => {
    const result: { [key: string]: any } = {}
    Object.keys(data).forEach(key => {
      const prop = data[key]
      if (prop === data) return
      if (typeof prop === 'object' && prop instanceof Array) {
        result[key] = objectDeepCopy(prop)
        return
      }
      if (prop && typeof prop === 'object' && !(prop instanceof Date)) {
        result[key] = fn(prop)
        return
      }
      result[key] = prop
    })
    return result
  }
  return Array.isArray(data) ? data.map(item => fn(item)) : fn(data)
}

export default {
  dateFormat,
  isInvalid,
  md5,
  generateUniqueId,
  getCookie,
  getLocalIPAddress,
  request,
  unzip,
  zip,
  removeDir,
  consoleBeautify,
  toDecimal,
  openURLBrowser,
  enCryp,
  deCryp,
  getAvailablePort,
  objectDeepCopy
}